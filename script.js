// user the "require" directive to load HTTP module of node JS
// A "module" is a software components or part of a program that contains one or more routines
// the 'HTTP module' lets Node.js transfer data using the HTTP(HyperText Transfer Protocol)
// HTTP is a protocol that tallows the fetching of resources such as html documents
// Clients (browser) and servers(nodeJS/ExpressJs applications) communicate by exchanging inidividual messages

// REQUEST - message sent by the client (usually in the WEB browser)
// RESPONSES - message sent by the serve as an answer to the client

let http=require("http")

//create a server object:
// the http module has a createServe()method that accepts a function as an argument and allows server creation
// the arguments passed in the function are request & response objects (date type) that contain methods that allows us to receive request from the client and send the response back
// using the module's createServer() method, we create an HTTP server that listens to the request on a specified port and give back to the client


http.createServer(function (req, res) {
    // port a virtual point where network connections start and end
// set a status code for reponse 
    res.writeHead(200, {'Content-Type': 'text/plain'});//end response
    res.write("Hellow ELliot"); 
    res.end();
})

server.listen(4000); //the server object listens on port 4000

console.log('Server running at localhost:4000');