/* 
/greeting"
*/
const http=require("http");
const { request } = require("https");
let url = require("url")
const port = 4000
const server= http.createServer((req, res) =>{
    if (req.url == '/greeting'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Hello greeting");

    }else if(req.url== '/homepage'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Hello home");

    }else
        res.writeHead(404,{'Content-Type':'text/plain'});
        res.end("page not available");
})
server.listen(port)
console.log(`Server running at localhost: ${port}`);


